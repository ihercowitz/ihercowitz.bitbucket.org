function spin() {
	return Math.floor(Math.random()*3)
}

function animate (slot, index) {
  var total = parseInt(slot.css("height")) * -1;
  slot.css({"margin-top":0})
       .animate({"margin-top": total + "px"}, {
                "duration": (index * 1000) + 6500});
}

function stopReel(slot) {
  var totalItems = parseInt(slot.css("height"))/150;
  var item = Math.floor(Math.random()* totalItems);
  var spinTo =  item * -150;

  slot.stop().css({"margin-top":spinTo+"px"});

  return parseInt($($(slot).children()[item]).attr("data")); 

}


function addSlots (slot, item) {  
   var reel0=["coffee maker", "teapot", "espresso machine"]
   var reel1=["coffee filter", "tea strainer", "espresso tamper"]
   var reel2=["coffee grounds", "loose tea", "ground espresso beans"] 
   for (var i=0; i < 150 * (item+1); i++) {
      var idx = spin();
      var reel = eval("reel" + item) 
      slot.append("<div class='item' data="+idx+">"+reel[idx]+"</div>");
   } 
}

addSlots($("#slot1 .items"), 0);
addSlots($("#slot2 .items"), 1);
addSlots($("#slot3 .items"), 2);

function play() {
  $("#result-info").removeAttr("class");
   var slot1 = $("#slot1 .items");
   var slot2 = $("#slot2 .items");
   var slot3 = $("#slot3 .items");

   animate(slot1, 0);
   animate(slot2, 1);
   animate(slot3, 2);

   var result = [];
   var prizes=["a Cup of Coffee", "a Cup of Tea", "an Espresso"]
   setInterval(function() {
      switch (result.length) {
         case 0:
             result[0] = stopReel(slot1);
             break;
         case 1:
             result[1] = stopReel(slot2);    
             break;
         case 2:
             result[2] = stopReel(slot3);    
            if (result[0]===result[1] && result[1]===result[2]) {
              $("#result-info").addClass("win").html("You won "+ prizes[result[0]] +"!");
            } else {
              $("#result-info").addClass("lose").html("You lose...");    
            }
            break;             
             break;
         }   
   }, 1300);

}

$("#play").click(function() {
   if (!$("#slot3 .items").is(':animated'))
      play();
});