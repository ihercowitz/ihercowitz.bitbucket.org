def set(variable, value, varmap):
    varmap[variable]=int(value)
 
def get(variable, varmap):
    try:
        ret=varmap[variable]
    except:
        ret="NULL"
    print(ret)
 
def unset(variable, varmap):
    del varmap[variable]
 
def numequalto(value, varmap):
    print(len([k for k,v in varmap.iteritems() if varmap[k]==int(value)]))
 
def begin(varmap, level):
    return REPL(varmap=varmap.copy(), level=(level+1))
    
def commit(varmap, level):
    if level > 0:
        return varmap
    else:
        print("NO TRANSACTION")

def rollback(varmap, level):
    if level == 0:
        print("NO TRANSACTION")
 
common_fn = {
   'SET':set,
   'GET':get,
   'UNSET':unset,
   'NUMEQUALTO':numequalto,
}
 
transaction_fn = {
   'BEGIN':begin,
   'COMMIT':commit,
   'ROLLBACK':rollback,
}
 

def eval_command(cmd, varmap, level):
    if cmd[0] == "END":
        return False
    elif cmd[0] == "ROLLBACK":
        if level > 0:
            return
        else:
            print("NO TRANSACTION")
    elif cmd[0] in common_fn:
        cmd.append(varmap)
        common_fn[cmd[0]](*cmd[1::])
    elif cmd[0] in transaction_fn:
        x = transaction_fn[cmd[0]](varmap, level)
        if type(x) == dict:
            varmap.update(x)
            while level > 0:
                return varmap
        else:
            if level > 0:
                return        
    else:
        print("Invalid command") 

def input():
    cmd=raw_input("=>> ")
    return cmd.split() if len(cmd) > 1 else [""]


def REPL(varmap, level=0):
    while True:
        return eval_command(input(), varmap, level)
 
if __name__=="__main__":
    try:
        REPL(dict())
    except:
        print("Goodbye")