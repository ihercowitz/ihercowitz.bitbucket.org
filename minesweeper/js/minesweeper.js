function bombs() {
    bpos = [];

    while (bpos.length < 10) {
        var pos = Math.floor(Math.random() * 64);
        if (bpos.indexOf(pos) === -1) {
            bpos.push(pos);
        }
    }

    return bpos;
}

function updateBombBoard(line) {
    return function (board, idxBomb) {
        var row = function (idx) {
           r = Math.ceil(idx / 8);
           if (idx % 8 !== 0)
              r = r - 1;
           return r;   
        };
        
        idx = idxBomb + line;

        if (idx >= 0 && idx <= 63 && line !== 0 && board[idx] != -1) 
            board[idx] = board[idx] + 1;

        var prev = idx - 1;
        var next = idx + 1;

        if (idx > 0 && board[prev] > -1 && row(prev) === row(idx)) 
            board[prev] = board[prev] + 1;

        if (idx < 63 && board[next] > -1  && row(next) === row(idx)) 
            board[next] = board[next] + 1;
    };
}

function generateGame() {
    var board = new Array(64);
    
    $.each(board, function(index) {
        board[index] = 0; 
    });
    
    var b = bombs().sort(function (a, b) {
        return a - b;
    });
    
    $.each(b, function (index, bomb) {
        board[bomb] = -1;
    });
    
    $.each(board, function (index, cell) {
        if (cell === -1) {
            currentRow(board, index);
            upperRow(board, index);
            bottomRow(board, index);
        }   
    });    

   $.each(board, function (index, cell) {
      $("#board").append("<div class='cell hidden' id="+index+"></div>"); 
   });    
    
   return board; 
}

function preloadImage() {
   img = new Image();
   img.src= 'img/bomb.jpg';
}

function start() {
  
    var board = generateGame();    

    $("#board").append("<input type='button' id='restart' value='New Game'>")
               .append("<input type='button' id='cheat' value='Cheat'>");
    
    $('.cell').click(function() {
      var itemID = $(this).attr('id');
      var item = board[itemID];  
       if (item === -1 ) {
            $("#"+itemID).empty();
            
            $.each(board, function(index, value) {
                if (value === -1)
                   $("#"+index).addClass('bomb-image');
                else
                    $("#"+index).text(value);                
            });
            $('.cell').toggleClass('hidden', 'lose').unbind();
       } else {
          $("#"+itemID).text(item);   
          $("#"+itemID).removeClass('cell');
          $("#"+itemID).addClass('clicked');
          $("#"+itemID).toggleClass('hidden', 'reveal').unbind();
          if ($('.cell').length === 10) {
              $.map($('.cell'), function (item) {
                $("#"+item.id).addClass('win');
              });
              $('.cell').toggleClass('hidden', 'reveal').unbind();

          }
       }  
    });
    
    $('#restart').click(function() {
       $('#board').empty();
       start();
    });

    $('#cheat').click(function() {
       $.each(board, function (index, value){
          if (value === -1) {
            $("#"+index).addClass('cheat');
          }
       });
    });

}

var currentRow = updateBombBoard(0);
var upperRow = updateBombBoard(-8);
var bottomRow = updateBombBoard(8);

preloadImage();
start();